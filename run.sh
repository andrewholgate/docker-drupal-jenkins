#!/bin/bash
set -e

## If you're mounting the .ssh volume you'll need to add the keys.
#chown jenkins /var/lib/jenkins/.ssh/id_rsa.bitbucket*
#chown jenkins /var/lib/jenkins/.ssh
#eval `ssh-agent -s`
#ssh-add /var/lib/jenkins/.ssh/id_rsa.bitbucket
#ssh-keyscan "bitbucket.com" >> /var/lib/jenkins/.ssh/known_hosts
#

# Jenkins needs to own project directory to create output files and directories.
chown -R jenkins:jenkins /var/lib/jenkins/project

exec supervisord -n -c /etc/supervisor/conf.d/supervisord.conf

exec "$@"
