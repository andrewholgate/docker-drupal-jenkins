FROM ubuntu:14.04
MAINTAINER Andrew Holgate <andrewholgate@yahoo.com>

RUN apt-get update
RUN apt-get -y upgrade

RUN DEBIAN_FRONTEND=noninteractive apt-get -y install supervisor wget curl htop nano php5-cli php5-dev php-pear software-properties-common

# Add Jenkins repository
RUN wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
RUN sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'

# Install and configure Jenkins.
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install jenkins
ENV JENKINS_URL http://localhost:8080
ENV JENKINS_CLI /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar

# Start Jenkins, wait 30 seconds for it to start then retreive latest update centre configurations and plugins.
RUN service jenkins start; \
    sleep 30; \
    curl  -L http://updates.jenkins-ci.org/update-center.json | sed '1d;$d' | curl -X POST -H 'Accept: application/json' -d @- $JENKINS_URL/updateCenter/byId/default/postBack; \
    java -jar $JENKINS_CLI -s $JENKINS_URL install-plugin analysis-core analysis-collector checkstyle dry phing plot pmd git doxygen htmlpublisher cloverphp crap4j jdepend xunit

# Install PHP Template for Jenkins by Sebastian Bergmann.
USER jenkins
RUN mkdir -p $HOME/jobs/php-template
RUN wget -P $HOME/jobs/php-template/ https://raw.github.com/sebastianbergmann/php-jenkins-template/master/config.xml
USER root

# Add latest git version via repo.
RUN add-apt-repository -y ppa:git-core/ppa
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install git

# Install XDebug.
RUN DEBIAN_FRONTEND=noninteractive pecl install xdebug
COPY xdebug.ini /etc/php5/cli/conf.d/xdebug.ini
RUN mkdir /tmp/xdebug && chown jenkins:jenkins /tmp/xdebug
RUN mkdir /var/log/xdebug && chown jenkins:jenkins /var/log/xdebug

# Install Composer
RUN echo "export COMPOSER_HOME=/var/lib/jenkins/.composer" >> /etc/bash.bashrc
ENV COMPOSER_HOME /var/lib/jenkins/.composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Install inspection tools via Composer.
ENV COMPOSER_HOME /var/lib/jenkins/.composer
RUN composer global require phing/phing:dev-master#37f1596ad959bb3b0bd19fa294ccba5c11b360f7
RUN composer global require pdepend/pdepend:~2.0
RUN composer global require phpmd/phpmd:~2.0
RUN composer global require sebastian/phpcpd:~2.0
RUN composer global require sebastian/phpdcd:~1.0
RUN composer global require phploc/phploc:~2.0
RUN composer global require squizlabs/php_codesniffer:~2.0
RUN composer global require drupal/coder:~8.0
RUN composer global require andrewholgate/drupalstrict:~0.1
RUN composer global require phpunit/phpunit:~4.0
RUN composer global require phpunit/php-invoker:~1.0
RUN composer global require phpunit/dbunit:~1.0
RUN composer global require phpunit/phpunit-selenium:~1.0
RUN composer global require phpunit/phpunit-skeleton-generator:~2.0
RUN composer global require halleck45/php-metrics:~1.0
RUN composer global require drush/drush:~6.0

# Symlink PHP Sniffer rules.
RUN ln -s $COMPOSER_HOME/vendor/drupal/coder/coder_sniffer/Drupal $COMPOSER_HOME/vendor/squizlabs/php_codesniffer/CodeSniffer/Standards/
RUN ln -s $COMPOSER_HOME/vendor/andrewholgate/drupalstrict/DrupalStrict $COMPOSER_HOME/vendor/squizlabs/php_codesniffer/CodeSniffer/Standards/
# Symlink Phing & Pdepend in order for Jenkins sh user to access it.
# @todo find a better solution.
RUN ln -s /var/lib/jenkins/.composer/vendor/bin/phing /usr/bin/ && \
    ln -s /var/lib/jenkins/.composer/vendor/bin/pdepend /usr/bin/ && \
    ln -s /var/lib/jenkins/.composer/vendor/bin/phpmd /usr/bin/ && \
    ln -s /var/lib/jenkins/.composer/vendor/bin/phpunit /usr/bin/

RUN chown -R jenkins:jenkins $COMPOSER_HOME

# Install Drush
RUN pear install Console_Table
RUN ln -s $COMPOSER_HOME/vendor/drush/drush/drush /usr/local/bin/drush

# Install documention tools.
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install python-sphinx python-pip doxygen
RUN DEBIAN_FRONTEND=noninteractive pip install sphinx_rtd_theme breathe

# PHP Configurations
RUN sed -ri 's/^display_errors\s*=\s*Off/display_errors = On/g' /etc/php5/cli/php.ini
RUN sed -ri 's/^display_startup_errors\s*=\s*Off/display_startup_errors = On/g' /etc/php5/cli/php.ini
RUN sed -ri 's/^error_reporting\s*=.*$/error_reporting = -1/g' /etc/php5/cli/php.ini

# Add global environmental variables.
RUN echo "export TERM=xterm" >> /etc/bash.bashrc
RUN echo "export PATH=/var/lib/jenkins/.composer/vendor/bin:$PATH" >> /etc/bash.bashrc

# Grant ubuntu user access to sudo with no password.
# TODO: This is for convenience and is insecure.
RUN apt-get -y install sudo
RUN echo "jenkins ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN usermod -a -G sudo jenkins

# Supervisor
RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY run.sh /usr/local/bin/run
RUN chmod +x /usr/local/bin/run

COPY jenkins.sh /usr/local/bin/jenkins
RUN chmod +x /usr/local/bin/jenkins

# Clean-up installation.
RUN DEBIAN_FRONTEND=noninteractive apt-get autoclean

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/run"]
