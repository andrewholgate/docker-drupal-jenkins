# Overview

A complete dockerized Drupal 7 & 8 Continuous Integration suite using [Jenkins CI](http://jenkins-ci.org/).

- Drupal code inspection using the [Drupal Coder](https://www.drupal.org/project/coder) best practices.
- PHP code inspection tools, such as [PHP Copy/Paste Detector (PHPCPD)](https://github.com/sebastianbergmann/phpcpd), etc.
- Drupal documentation generator in [Read The Docs](http://docs.readthedocs.org/en/latest/) format using [Sphinx](http://sphinx-doc.org/).
- Project build using [Phing](http://www.phing.info/).
- PHP tests using [PHPUnit](https://phpunit.de/).

# Installation

This assumes that you have [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) installed for managing containerisation and their orchestration.

```bash
# Clone this repository
git clone https://github.com/andrewholgate/docker-drupal-jenkins.git
cd docker-drupal-jenkins
# Build and start container.
sudo docker-compose build | tee ./build.log
sudo docker-compose up -d
```

# Usage

1. Navigate to http://localhost:8080/ to access the Jenkins CI interface.
2. Or login to the Jenkins server using:

```bash
# Login to Docker container as root user
sudo docker exec -it dockerdrupaljenkins_drupaljenkins_1 su - jenkins
```

## Phing and Drupal

In order to get the most out of this Jenkins CI stack, you will need an understanding of [how to use Phing with Drupal](https://www.lullabot.com/blog/article/automate-your-life-phing) and how to customise a [Phing Drupal project template](https://github.com/reload/phing-drupal-template) in order to report job targets to Jenkins. Read [Template for Jenkins Jobs for PHP Projects](http://jenkins-php.org/) for more information on using Jenkins with PHP.

# Included Tools

## Code Inspection Tools

- [Drupal Coder](https://www.drupal.org/project/coder) - defines Drupal coding standards.
- [Drupal Strict](https://github.com/andrewholgate/drupalstrict) - defines strict Drupal and PHP coding standards.
- [PHP Code Sniffer](https://github.com/squizlabs/PHP_CodeSniffer) - detect violations of coding standards.
- [PHP Mess Detector (PHPMD)](http://phpmd.org/) - discover possible bugs and suboptimal code.
- [PHP Copy/Paste Detector (PHPCPD)](https://github.com/sebastianbergmann/phpcpd) - detect copy/pasted code.
- [PHP Dead Code Detector (PHPDCD)](https://github.com/sebastianbergmann/phpdcd) - discover unused PHP code.
- [PHP Lines of Code (PHPLoC)](https://github.com/sebastianbergmann/phploc) - measuring the size of a project.
- [PHP Depend (PHPMD)](http://pdepend.org/) - generate design quality metrics.
- [Phing](http://www.phing.info/) - project build tool.
- [PHPUnit](https://phpunit.de/) - unit testing PHP code.
- [PHP Metrics](http://www.phpmetrics.org/) - visualisation of code inspection tools.

## PHP Documentation Tools

- [DoxyGen](http://www.doxygen.org) - generate documentation from annotated PHP code. It is used to generate XML which is then interpreted by Sphinx.
- [Sphinx](http://sphinx-doc.org/) - generate beautiful [Read The Docs](http://docs.readthedocs.org/en/latest/) format using [Breathe](https://breathe.readthedocs.org/) as a bridge to DoxyGen XML output.

## Jenkins Plugins

- [Analysis Collector](https://wiki.jenkins-ci.org/display/JENKINS/Analysis+Collector+Plugin) - collect and graph various generated files showing trends.
- [Static Code Analysis](https://wiki.jenkins-ci.org/display/JENKINS/Static+Code+Analysis+Plug-ins) - process static code generated files.
- [Phing](https://wiki.jenkins-ci.org/display/JENKINS/Phing+Plugin) - allow for integration with Phing to build projects.
- [Checkstyle](https://wiki.jenkins-ci.org/display/JENKINS/Checkstyle+Plugin) - process PHP_CodeSniffer generated files in Checkstyle format.
- [DRY](https://wiki.jenkins-ci.org/display/JENKINS/DRY+Plugin) - process PHPCPD generated files.
- [Plot](https://wiki.jenkins-ci.org/display/JENKINS/Plot+Plugin) - process PHPLoC generated files.
- [PMD](https://wiki.jenkins-ci.org/display/JENKINS/PMD+Plugin) - process PHP Mess Detector generated files.
- [Clover PHP](https://wiki.jenkins-ci.org/display/JENKINS/Clover+PHP+Plugin)- process PHPUnit Clover generated file.
- [Crap4J](https://wiki.jenkins-ci.org/display/JENKINS/Crap4J+Plugin) - process PHPUnit Crap4J generated file.
- [JDepend](https://wiki.jenkins-ci.org/display/JENKINS/JDepend+Plugin) - process PHP Depend generated files.
- [xUnit](https://wiki.jenkins-ci.org/display/JENKINS/xUnit+Plugin) - process PHPUnit JUnit generated file.
- [DoxyGen](https://wiki.jenkins-ci.org/display/JENKINS/Doxygen+Plugin) -  generate Doxygen documentation and publish reports.
- [HTML Publisher](https://wiki.jenkins-ci.org/display/JENKINS/HTML+Publisher+Plugin) - publish and archive HTML reports and documentation.
- [Git](https://wiki.jenkins-ci.org/display/JENKINS/Git+Plugin) - allow for integration with Git repositories.

### Jenkins Job Templates

- [Sebastian Bergmann's PHP Jenkins Template](https://github.com/sebastianbergmann/php-jenkins-template/)

## Other Tools

- [Drush](https://github.com/drush-ops/drush) - command-line shell and scripting interface for Drupal.
- [Composer](https://getcomposer.org/) - dependency package manager for PHP.
- [XDebug](http://www.xdebug.org/) - used by PHPUnit for code inspection.

# Learn More About Jenkins CI for Drupal

- [Jenkins & Drupal Development](https://wiki.jenkins-ci.org/display/JENKINS/Drupal+Development)

# Roadmap

- Install https://wiki.jenkins-ci.org/display/JENKINS/OWASP+Markup+Formatter+Plugin
  - antisamy-markup-formatter
  - Configure here: http://localhost:8080/configureSecurity/
- Add JS & CSS linting tools, such as https://github.com/CSSLint/csslint
- Uninstall unnecessary Jenkins plugins.
- Move Composer installed inspection tools into a seperate project.
- Implement [Jenkins security improvements](https://wiki.jenkins-ci.org/display/JENKINS/Securing+Jenkins).
