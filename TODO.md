# Project jobs

For newly imported projects:

1. Need to copy over the jobs template from the project into `jobs/project-name`
1. Then on http://localhost:8080/manage select `Reload Configuration from Disk`
